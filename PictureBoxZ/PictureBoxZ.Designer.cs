﻿namespace PictureBoxZ
{
    partial class PictureBoxZ
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxAnim = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAnim)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxAnim
            // 
            this.pictureBoxAnim.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxAnim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxAnim.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxAnim.MaximumSize = new System.Drawing.Size(1024, 1024);
            this.pictureBoxAnim.MinimumSize = new System.Drawing.Size(8, 8);
            this.pictureBoxAnim.Name = "pictureBoxAnim";
            this.pictureBoxAnim.Size = new System.Drawing.Size(150, 150);
            this.pictureBoxAnim.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxAnim.TabIndex = 0;
            this.pictureBoxAnim.TabStop = false;
            this.pictureBoxAnim.Visible = false;
            this.pictureBoxAnim.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxAnim_Paint);
            this.pictureBoxAnim.Resize += new System.EventHandler(this.pictureBoxAnim_Resize);
            // 
            // PictureBoxZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.pictureBoxAnim);
            this.DoubleBuffered = true;
            this.Name = "PictureBoxZ";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBoxZ_MouseDown);
            this.MouseEnter += new System.EventHandler(this.PictureBoxZ_MouseEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBoxZ_MouseMove_MouseUp);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureBoxZ_MouseMove_MouseUp);
            this.Resize += new System.EventHandler(this.PictureBoxZ_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAnim)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxAnim;
    }
}
